#pragma once
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <btBulletDynamicsCommon.h>
#include "Clone/PhysicsObject.hpp"

namespace cl
{

class BulletPhysicsWorld;

struct PhysicAddresses
{
    void *trigger;
    void *physics;
};

class BulletPhysicsObject : public cl::PhysicsObject
{
 friend class BulletPhysicsWorld;
 public:
    BulletPhysicsObject(btCollisionShape *shape, float mass, const glm::quat &orientation, const glm::vec3 &position);
    BulletPhysicsObject(const BulletPhysicsObject &obj);
    ~BulletPhysicsObject();
    BulletPhysicsObject& operator=(const BulletPhysicsObject &obj);

    void setPosition(const glm::vec3 &position);
    glm::vec3 getPosition() const;

    void setRotation(const glm::vec3 &rotation);
    glm::vec3 getRotation() const;

    void setVelocity(const glm::vec3 &velocity);
    glm::vec3 getVelocity() const;

    void setTransformMatrix(const glm::mat4 &matrix);
    glm::mat4 getTransformMatrix() const;

    void setTriggerPointer(void *pointer);
    void* getTriggerPointer();

    void setTriggerIndex(int index);
    int getTriggerIndex() const;

    void setGravity(const glm::vec3 &gravity);
    glm::vec3 getGravity() const;

    void setLinearFactor(const glm::vec3 &linear_factor);
    glm::vec3 getLinearFactor() const;

    void setAngularFactor(const glm::vec3 &angular_factor);
    glm::vec3 getAngularFactor() const;

    void setDamping(float linear_damping, float angular_damping);
    float getLinearDamping() const;
    float getAngularDamping() const;

    void setMass(float mass);
    float getMass() const;

    void setRestitution(float restitution);
    float getRestitution() const;

    void setFriction(float friction);
    float getFriction() const;

    void setRollingFriction(float rolling_friction);
    float getRollingFriction() const;

    btRigidBody* getRigidBody();
    cl::BulletPhysicsWorld* getParentWorld();
    virtual void removeFromParentWorld();

 protected:
    cl::BulletPhysicsWorld *parent_world_;

 private:
    btCollisionShape *shape_;
    btDefaultMotionState motion_state_;
    btRigidBody *rigid_body_;
    float mass_;

    cl::PhysicAddresses addresses_;

    void deconstruct();
};

}
