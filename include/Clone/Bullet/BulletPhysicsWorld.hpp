#pragma once
#include <vector>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include "Clone/Bullet/BulletCharacterObject.hpp"
#include "Clone/Bullet/BulletDebugDraw.hpp"
#include "Clone/Bullet/BulletTriggerObject.hpp"
#include "Clone/Bullet/BulletCollisions.hpp"
#include "Clone/Shader.hpp"
#include "Clone/Camera.hpp"

namespace cl
{

struct ContactCallbackData;

class BulletPhysicsWorld
{
 public:
    BulletPhysicsWorld();
    ~BulletPhysicsWorld();

    void simulate(float dt);
    void intersect(cl::BulletTriggerObject *obj);
    void intersect(cl::BulletTriggerObject *obj, short col_group, short col_mask);
    bool hasIntersected(cl::BulletPhysicsObject *A, cl::BulletPhysicsObject *B, cl::ContactCallbackData *cb_data);
    bool rayCast(const glm::vec3 &start, const glm::vec3 &end, float *distance);
    bool rayCast(const glm::vec3 &start, const glm::vec3 &end, float *distance, short col_group, short col_mask);
    void rayCastAll(const glm::vec3 &start, const glm::vec3 &end, std::vector<glm::vec3> *output_points);
    void rayCastAll(const glm::vec3 &start, const glm::vec3 &end, std::vector<glm::vec3> *output_points, short col_group, short col_mask);
    void addObject(cl::BulletPhysicsObject *obj);
    void addObject(cl::BulletPhysicsObject *obj, short col_group, short col_mask);
    void addCharacter(cl::BulletCharacterObject *obj);
    void addCharacter(cl::BulletCharacterObject *obj, short col_group, short col_mask);
    void addCollisions(cl::BulletCollisions *col);
    void addCollisions(cl::BulletCollisions *col, short col_group, short col_mask);
    void removeObject(cl::BulletPhysicsObject *obj);
    void removeCharacter(cl::BulletCharacterObject *obj);
    void removeCollisions(cl::BulletCollisions *col);
    void initDebug(cl::Shader *debug_shader);
    void drawAllDebug(cl::Camera *camera);
    void drawDebug(cl::BulletPhysicsObject *obj, cl::Camera *camera);

    void setGravity(const glm::vec3 &gravity);
    const glm::vec3& getGravity() const;

    btDiscreteDynamicsWorld* getDynamicsWorld();

 private:
    BulletPhysicsWorld(const cl::BulletPhysicsWorld &obj);
    BulletPhysicsWorld& operator=(const cl::BulletPhysicsWorld &obj);

    btDbvtBroadphase *broadphase_;
    btDefaultCollisionConfiguration *collision_config_;
    btCollisionDispatcher *dispatcher_;
    btSequentialImpulseConstraintSolver *solver_;
    btDiscreteDynamicsWorld *dynamics_world_;
    btGhostPairCallback *ghost_callback_;

    cl::Shader *debug_shader_;
    cl::BulletDebugDraw debug_draw_;
    glm::vec3 gravity_;

    std::vector<cl::BulletCharacterObject*> characters_;
};

struct ContactCallbackData
{
    cl::BulletPhysicsObject *A_obj;
    cl::BulletPhysicsObject *B_obj;

    std::vector<glm::vec3> A_contacts;
    std::vector<glm::vec3> B_contacts;
};

struct ContactCallback : public btCollisionWorld::ContactResultCallback
{
    ContactCallback();

    bool found;
    cl::ContactCallbackData *data;

    btScalar addSingleResult(btManifoldPoint &cp, 
        const btCollisionObjectWrapper *colObj0, int partId0, int index0,
        const btCollisionObjectWrapper *colObj1, int partId1, int index1);
};


}
