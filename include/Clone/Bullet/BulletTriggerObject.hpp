#pragma once
#include <vector>
#include "Clone/Bullet/BulletPhysicsObject.hpp"

namespace cl
{

struct TriggerCallbackData
{
    void *pointer;
    int index;
};

class BulletTriggerObject : public cl::BulletPhysicsObject
{
 public:
    BulletTriggerObject(btCollisionShape *shape, float mass, const glm::quat &orientation, const glm::vec3 &position);
    BulletTriggerObject(const cl::BulletTriggerObject &obj);
    ~BulletTriggerObject();
    BulletTriggerObject& operator=(const cl::BulletTriggerObject &obj);

    bool intersected() const;
    bool pollHitObjects(TriggerCallbackData *output);
    void clear();
    std::vector<TriggerCallbackData>* getHitObjects();

 private:
    std::vector<TriggerCallbackData> hit_objects_;
    size_t poll_index_;
};

struct TriggerCallback : public btCollisionWorld::ContactResultCallback
{
    TriggerCallback(cl::BulletTriggerObject *trig);

    cl::BulletTriggerObject *trigger;

    btScalar addSingleResult(btManifoldPoint &cp,
        const btCollisionObjectWrapper *colObj0, int partId0, int index0,
        const btCollisionObjectWrapper *colObj1, int partId1, int index1);
};

}
