#pragma once
#include <vector>
#include <string>
#include "Clone/Bullet/BulletPhysicsObject.hpp"

namespace cl
{

class BulletCollisions
{
 friend class BulletPhysicsWorld;
 public:
    BulletCollisions();
    ~BulletCollisions();

    void initialize(const std::string &file_contents);

    std::vector<cl::BulletPhysicsObject>* getObjects();

 private:
    BulletCollisions(const cl::BulletCollisions &obj);
    BulletCollisions& operator=(const cl::BulletCollisions &obj);

    std::vector<btBoxShape> box_shapes_;
    std::vector<btCylinderShape> cylinder_shapes_;
    std::vector<cl::BulletPhysicsObject> objects_;
};

}
