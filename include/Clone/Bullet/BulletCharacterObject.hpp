#pragma once
#include "Clone/Bullet/BulletRayCallback.hpp"
#include "Clone/Bullet/BulletPhysicsObject.hpp"

namespace cl
{

class BulletCharacterObject : public cl::BulletPhysicsObject
{
 public:
    BulletCharacterObject(btCollisionShape *shape, float height, float mass, const glm::quat &orientation, const glm::vec3 &position);
    BulletCharacterObject(const BulletCharacterObject &obj);
    ~BulletCharacterObject();
    BulletCharacterObject& operator=(const BulletCharacterObject &obj);

    void preStep();
    void postStep();

    void jump();

    void setJumpVelocity(const glm::vec3 &jump_velocity);
    glm::vec3 getJumpVelocity() const;

    void setStepSize(float step_size);
    float getStepSize() const;

    bool onGround() const;
    void removeFromParentWorld();

 private:
    float height_;
    float step_size_;
    glm::vec3 jump_velocity_;
    bool high_slope_;
    bool jumping_;
    bool on_ground_;
    glm::vec3 surface_normal_;
};

}
