#pragma once
#include <SDL2/SDL_log.h>
#include <btBulletDynamicsCommon.h>
#include <LinearMath/btIDebugDraw.h>
#include <glm/vec3.hpp>
#include "Clone/VertexObject.hpp"
#include "Clone/Shader.hpp"

namespace cl
{

class BulletDebugDraw : public btIDebugDraw
{
 public:
    BulletDebugDraw();
    virtual ~BulletDebugDraw();

    void initialize(cl::Shader *shader);

    virtual void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color);
    virtual void drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int life_time, const btVector3 &color);
    virtual void reportErrorWarning(const char *warning_string);
    virtual void draw3dText(const btVector3 &location, const char *text_string);
    virtual void setDebugMode(int debug_mode);
    virtual int getDebugMode() const;

 private:
    BulletDebugDraw(const cl::BulletDebugDraw &obj);
    BulletDebugDraw& operator=(const cl::BulletDebugDraw &obj);

    int debug_mode_;
    cl::Shader *shader_;
    cl::VertexObject vertex_object_;
};

}
