#pragma once
#include <btBulletDynamicsCommon.h>

namespace cl
{

struct BulletRayCallbackAll : public btCollisionWorld::AllHitsRayResultCallback
{
    BulletRayCallbackAll(const btVector3 &from, const btVector3 &to) :
        btCollisionWorld::AllHitsRayResultCallback(from, to)
    {

    }

    bool needsCollision(btBroadphaseProxy *proxy0) const
    {
        bool collides = (proxy0->m_collisionFilterGroup & m_collisionFilterMask) != 0;
        collides = collides && (m_collisionFilterGroup & proxy0->m_collisionFilterMask);

        if (m_collisionObject != NULL)
            collides = collides && !(m_collisionObject->getCollisionFlags() & btCollisionObject::CF_NO_CONTACT_RESPONSE);

        return collides;
    }
};

struct BulletRayCallback : public btCollisionWorld::ClosestRayResultCallback
{
    BulletRayCallback(const btVector3 &from, const btVector3 &to) :
        btCollisionWorld::ClosestRayResultCallback(from, to)
    {

    }

    bool needsCollision(btBroadphaseProxy *proxy0) const
    {
        bool collides = (proxy0->m_collisionFilterGroup & m_collisionFilterMask) != 0;
        collides = collides && (m_collisionFilterGroup & proxy0->m_collisionFilterMask);

        if (m_collisionObject != NULL)
            collides = collides && !(m_collisionObject->getCollisionFlags() & btCollisionObject::CF_NO_CONTACT_RESPONSE);

        return collides;
    }
};

}
