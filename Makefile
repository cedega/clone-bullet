INCLUDE_CLONE=../clone/include/
INCLUDE=include/
CFLAGS=-g -O3
FRAMEWORKS=

# LIBS: -lclone_bullet -lBulletDynamics -lBulletCollision -lLinearMath
# INC:  -I"../clone-bullet/include/" -I"/usr/local/include/bullet/"

ifeq ($(OS),Windows_NT)

	MESSAGE="Operating System Detected: Windows"
	COMPILE=lib/windows/
	BULLET_INC=$(INCLUDE)bullet/

else ifeq ($(shell uname), Darwin)

	MESSAGE="Operating System Detected: Mac OSX"
	COMPILE=lib/mac/
	FRAMEWORKS="-Flib/mac/"
	BULLET_INC=$(INCLUDE)bullet/

else

	MESSAGE="Operating System Detected: Linux"
	COMPILE=lib/linux/
	BULLET_INC=/usr/local/include/bullet/

endif

srcs = $(wildcard src/Bullet/*.cpp)
objs = $(srcs:.cpp=.o)
deps = $(srcs:.cpp=.d)

LIBNAME=libclone_bullet.a
OBJECTS=src/Bullet/*.o

all: lib

lib: $(objs)
	@echo $(MESSAGE)
	ar rcs $(COMPILE)$(LIBNAME) $(OBJECTS)

%.o: %.cpp
	g++ $(CFLAGS) -I"$(INCLUDE)" -I"$(INCLUDE_CLONE)" -I"$(BULLET_INC)" -L"$(COMPILE)" -MMD -MP -Wall -c $< -o $@

clean:
	rm -f src/Bullet/*.o src/Bullet/*.d $(COMPILE)$(LIBNAME)

-include $(deps)
