#include <Clone/Bullet/BulletPhysicsWorld.hpp>

namespace cl
{

BulletPhysicsWorld::BulletPhysicsWorld()
{
    broadphase_ = new btDbvtBroadphase();
    ghost_callback_ = new btGhostPairCallback();
    broadphase_->getOverlappingPairCache()->setInternalGhostPairCallback(ghost_callback_);

    collision_config_ = new btDefaultCollisionConfiguration();
    dispatcher_ = new btCollisionDispatcher(collision_config_);
    solver_ = new btSequentialImpulseConstraintSolver();
    dynamics_world_ = new btDiscreteDynamicsWorld(dispatcher_, broadphase_, solver_, collision_config_);
}

BulletPhysicsWorld::~BulletPhysicsWorld()
{
    for (int i = dynamics_world_->getNumCollisionObjects() - 1; i >= 0; i--)
    {
        btCollisionObject* obj = dynamics_world_->getCollisionObjectArray()[i];
        cl::PhysicAddresses *pa = static_cast<cl::PhysicAddresses*>(obj->getUserPointer());
        cl::BulletPhysicsObject *bpo = static_cast<cl::BulletPhysicsObject*>(pa->physics);
        bpo->deconstruct();
    }

    delete dynamics_world_;
    delete solver_;
    delete collision_config_;
    delete dispatcher_;
    delete ghost_callback_;
    delete broadphase_;
}

void BulletPhysicsWorld::simulate(float dt)
{
    for (unsigned int i = 0; i < characters_.size(); i++)
        characters_[i]->preStep();

    int steps = std::ceil(dt * 60.0f) + 1;
    dynamics_world_->stepSimulation(dt, steps, 1.0f / 60.0f);

    for (unsigned int i = 0; i < characters_.size(); i++)
        characters_[i]->postStep();
}

void BulletPhysicsWorld::intersect(cl::BulletTriggerObject *obj)
{
    cl::TriggerCallback callback(obj);
    obj->clear();
    dynamics_world_->contactTest(obj->getRigidBody(), callback);
}

void BulletPhysicsWorld::intersect(cl::BulletTriggerObject *obj, short col_group, short col_mask)
{
    cl::TriggerCallback callback(obj);
    callback.m_collisionFilterGroup = col_group;
    callback.m_collisionFilterMask = col_mask;
    obj->clear();
    dynamics_world_->contactTest(obj->getRigidBody(), callback);
}

bool BulletPhysicsWorld::hasIntersected(cl::BulletPhysicsObject *A, cl::BulletPhysicsObject *B, cl::ContactCallbackData *cb_data)
{
    cl::ContactCallback cb;

    if (cb_data != NULL)
    {
        cb_data->A_obj = A;
        cb_data->B_obj = B;
        cb_data->A_contacts.reserve(8);
        cb_data->B_contacts.reserve(8);
        cb.data = cb_data;
    }

    dynamics_world_->contactPairTest(A->getRigidBody(), B->getRigidBody(), cb);
    return cb.found;
}

bool BulletPhysicsWorld::rayCast(const glm::vec3 &start, const glm::vec3 &end, float *distance)
{
    btVector3 from(start.x, start.y, start.z);
    btVector3 to(end.x, end.y, end.z);

    cl::BulletRayCallback cb(from, to);
    dynamics_world_->rayTest(from, to, cb);

    if (cb.hasHit())
    {
        if (distance != NULL)
            *distance = (cb.m_hitPointWorld - cb.m_rayFromWorld).length();

        return true;
    }

    return false;
}

bool BulletPhysicsWorld::rayCast(const glm::vec3 &start, const glm::vec3 &end, float *distance, short col_group, short col_mask)
{
    btVector3 from(start.x, start.y, start.z);
    btVector3 to(end.x, end.y, end.z);

    cl::BulletRayCallback cb(from, to);
    cb.m_collisionFilterGroup = col_group;
    cb.m_collisionFilterMask = col_mask;

    dynamics_world_->rayTest(from, to, cb);

    if (cb.hasHit())
    {
        if (distance != NULL)
            *distance = (cb.m_hitPointWorld - cb.m_rayFromWorld).length();

        return true;
    }

    return false;
}

void BulletPhysicsWorld::rayCastAll(const glm::vec3 &start, const glm::vec3 &end, std::vector<glm::vec3> *output_points)
{
    btVector3 from(start.x, start.y, start.z);
    btVector3 to(end.x, end.y, end.z);

    cl::BulletRayCallbackAll cb(from, to);
    dynamics_world_->rayTest(from, to, cb);

    if (cb.hasHit())
    {
        output_points->resize(cb.m_hitPointWorld.size());

        for (unsigned int i = 0; i < output_points->size(); i++)
        {
            btVector3 &point = cb.m_hitPointWorld[i];
            output_points->at(i) = glm::vec3(point.getX(), point.getY(), point.getZ());
        }
    }
}

void BulletPhysicsWorld::rayCastAll(const glm::vec3 &start, const glm::vec3 &end, std::vector<glm::vec3> *output_points, short col_group, short col_mask)
{
    btVector3 from(start.x, start.y, start.z);
    btVector3 to(end.x, end.y, end.z);

    cl::BulletRayCallbackAll cb(from, to);
    cb.m_collisionFilterGroup = col_group;
    cb.m_collisionFilterMask = col_mask;

    dynamics_world_->rayTest(from, to, cb);

    if (cb.hasHit())
    {
        output_points->resize(cb.m_hitPointWorld.size());

        for (unsigned int i = 0; i < output_points->size(); i++)
        {
            btVector3 &point = cb.m_hitPointWorld[i];
            output_points->at(i) = glm::vec3(point.getX(), point.getY(), point.getZ());
        }
    }
}

void BulletPhysicsWorld::addObject(cl::BulletPhysicsObject *obj)
{
    dynamics_world_->addRigidBody(obj->rigid_body_);
    obj->parent_world_ = this;
}

void BulletPhysicsWorld::addObject(cl::BulletPhysicsObject *obj, short col_group, short col_mask)
{
    dynamics_world_->addRigidBody(obj->rigid_body_, col_group, col_mask);
    obj->parent_world_ = this;
}

void BulletPhysicsWorld::addCharacter(cl::BulletCharacterObject *obj)
{
    addObject(obj);
    characters_.push_back(obj);
}

void BulletPhysicsWorld::addCharacter(cl::BulletCharacterObject *obj, short col_group, short col_mask)
{
    addObject(obj, col_group, col_mask);
    characters_.push_back(obj);
}

void BulletPhysicsWorld::addCollisions(cl::BulletCollisions *col)
{
    for (size_t i = 0; i < col->objects_.size(); i++)
    {
        dynamics_world_->addRigidBody(col->objects_[i].rigid_body_);
        col->objects_[i].parent_world_ = this;
    }
}

void BulletPhysicsWorld::addCollisions(cl::BulletCollisions *col, short col_group, short col_mask)
{
    for (size_t i = 0; i < col->objects_.size(); i++)
    {
        dynamics_world_->addRigidBody(col->objects_[i].rigid_body_, col_group, col_mask);
        col->objects_[i].parent_world_ = this;
    }
}

void BulletPhysicsWorld::removeObject(cl::BulletPhysicsObject *obj)
{
    dynamics_world_->removeRigidBody(obj->rigid_body_);
    obj->parent_world_ = NULL;
}

void BulletPhysicsWorld::removeCharacter(cl::BulletCharacterObject *obj)
{
    removeObject(obj);

    for (unsigned int i = 0; i < characters_.size(); i++)
    {
        if (obj == characters_[i])
        {
            characters_.erase(characters_.begin() + i);
            break;
        }
    }
}

void BulletPhysicsWorld::removeCollisions(cl::BulletCollisions *col)
{
    for (size_t i = 0; i < col->objects_.size(); i++)
    {
        dynamics_world_->removeRigidBody(col->objects_[i].rigid_body_);
        col->objects_[i].parent_world_ = NULL;
    }
}

void BulletPhysicsWorld::initDebug(cl::Shader *debug_shader)
{
    debug_shader_ = debug_shader;
    debug_draw_.initialize(debug_shader_);

    dynamics_world_->setDebugDrawer(&debug_draw_);
    dynamics_world_->getDebugDrawer()->setDebugMode(btIDebugDraw::DBG_DrawWireframe);
}

void BulletPhysicsWorld::drawAllDebug(cl::Camera *camera)
{
    if (camera != NULL)
    {
        debug_shader_->setUniformM44v("cl_proj", &camera->getProjectionMatrix(), 1);
        debug_shader_->setUniformM44v("cl_view", &camera->getViewMatrix(), 1);
    }

    dynamics_world_->debugDrawWorld();
}

void BulletPhysicsWorld::drawDebug(cl::BulletPhysicsObject *obj, cl::Camera *camera)
{
    if (camera != NULL)
    {
        debug_shader_->setUniformM44v("cl_proj", &camera->getProjectionMatrix(), 1);
        debug_shader_->setUniformM44v("cl_view", &camera->getViewMatrix(), 1);
    }

    dynamics_world_->debugDrawObject(obj->getRigidBody()->getCenterOfMassTransform(), obj->getRigidBody()->getCollisionShape(), btVector3(0.0f, 0.0f, 0.0f));
}

void BulletPhysicsWorld::setGravity(const glm::vec3 &gravity)
{
    gravity_ = gravity;
    dynamics_world_->setGravity(btVector3(gravity_.x, gravity_.y, gravity_.z));
}

const glm::vec3& BulletPhysicsWorld::getGravity() const
{
    return gravity_;
}

btDiscreteDynamicsWorld* BulletPhysicsWorld::getDynamicsWorld()
{
    return dynamics_world_;
}

ContactCallback::ContactCallback() :
    btCollisionWorld::ContactResultCallback(),
    found(false),
    data(NULL)
{

}

btScalar ContactCallback::addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0, int partId0, int index0, const btCollisionObjectWrapper *colObj1, int partId1, int index1)
{
    found = true;

    if (data != NULL)
    {
        btCollisionObject *A = static_cast<btCollisionObject*>(data->A_obj->getRigidBody());

        if (A == colObj0->m_collisionObject)
        {
            glm::vec3 a_pos(cp.m_localPointA.getX(), cp.m_localPointA.getY(), cp.m_localPointA.getZ());
            glm::vec3 b_pos(cp.m_localPointB.getX(), cp.m_localPointB.getY(), cp.m_localPointB.getZ());

            data->A_contacts.push_back(a_pos);
            data->B_contacts.push_back(b_pos);
        }
        else if (A == colObj1->m_collisionObject)
        {
            glm::vec3 a_pos(cp.m_localPointB.getX(), cp.m_localPointB.getY(), cp.m_localPointB.getZ());
            glm::vec3 b_pos(cp.m_localPointA.getX(), cp.m_localPointA.getY(), cp.m_localPointA.getZ());

            data->A_contacts.push_back(a_pos);
            data->B_contacts.push_back(b_pos);
        }
    }

    return 0;
}

}
