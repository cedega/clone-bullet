#include <Clone/Bullet/BulletCollisions.hpp>

namespace cl
{

BulletCollisions::BulletCollisions()
{

}

BulletCollisions::~BulletCollisions()
{

}

void BulletCollisions::initialize(const std::string &file_contents)
{
    size_t start = 0;
    std::string line;

    int shape = -1;
    glm::vec3 position;
    glm::vec3 radius;
    glm::vec3 rotation;

    int index = 0;
    int runs = 0;

    int box_index = 0;
    int cylinder_index = 0;
    
    while (true)
    {
        size_t endline = file_contents.find('\n', start);
        line = file_contents.substr(start, endline - start);

        if (runs == 0)
        {
            box_shapes_.resize(atoi(line.c_str()), btBoxShape(btVector3(0.0, 0.0, 0.0)));
        }
        else if (runs == 1)
        {
            cylinder_shapes_.resize(atoi(line.c_str()), btCylinderShape(btVector3(0.0, 0.0, 0.0)));
        }
        else if (line.compare("END") == 0)
        {
            if (shape == 0)
            {
                box_shapes_[box_index] = btBoxShape(btVector3(radius.x, radius.y, radius.z));
                objects_.push_back(cl::BulletPhysicsObject(&box_shapes_[box_index], 0.0f, glm::quat(rotation), position));
                box_index++;
            }
            else if (shape == 1)
            {
                cylinder_shapes_[cylinder_index] = btCylinderShape(btVector3(radius.x, radius.y, radius.z));
                objects_.push_back(cl::BulletPhysicsObject(&cylinder_shapes_[cylinder_index], 0.0f, glm::quat(rotation), position));
                cylinder_index++;
            }

            index = 0;
            break;
        }
        else if (line.compare("BOX") == 0)
        {
            if (shape == 0)
            {
                box_shapes_[box_index] = btBoxShape(btVector3(radius.x, radius.y, radius.z));
                objects_.push_back(cl::BulletPhysicsObject(&box_shapes_[box_index], 0.0f, glm::quat(rotation), position));
                box_index++;
            }
            else if (shape == 1)
            {
                cylinder_shapes_[cylinder_index] = btCylinderShape(btVector3(radius.x, radius.y, radius.z));
                objects_.push_back(cl::BulletPhysicsObject(&cylinder_shapes_[cylinder_index], 0.0f, glm::quat(rotation), position));
                cylinder_index++;
            }

            index = 0;
            shape = 0;
        }
        else if (line.compare("CYLINDER") == 0)
        {
            if (shape == 0)
            {
                box_shapes_[box_index] = btBoxShape(btVector3(radius.x, radius.y, radius.z));
                objects_.push_back(cl::BulletPhysicsObject(&box_shapes_[box_index], 0.0f, glm::quat(rotation), position));
                box_index++;
            }
            else if (shape == 1)
            {
                cylinder_shapes_[cylinder_index] = btCylinderShape(btVector3(radius.x, radius.y, radius.z));
                objects_.push_back(cl::BulletPhysicsObject(&cylinder_shapes_[cylinder_index], 0.0f, glm::quat(rotation), position));
                cylinder_index++;
            }

            index = 0;
            shape = 1;
        }
        else
        {
            switch (index)
            {
            case 0: position.x = atof(line.c_str()); index++; break;
            case 1: position.y = atof(line.c_str()); index++; break;
            case 2: position.z = atof(line.c_str()); index++; break;
            case 3: radius.x = atof(line.c_str()); index++; break;
            case 4: radius.y = atof(line.c_str()); index++; break;
            case 5: radius.z = atof(line.c_str()); index++; break;
            case 6: rotation.x = atof(line.c_str()); index++; break;
            case 7: rotation.y = atof(line.c_str()); index++; break;
            case 8: rotation.z = atof(line.c_str()); index++; break;
            default: break;
            }
        }

        start = endline + 1;
        runs++;
    }
}

std::vector<cl::BulletPhysicsObject>* BulletCollisions::getObjects()
{
    return &objects_;
}

}
