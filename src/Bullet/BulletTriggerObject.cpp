#include <Clone/Bullet/BulletTriggerObject.hpp>

namespace cl
{

BulletTriggerObject::BulletTriggerObject(btCollisionShape *shape, float mass, const glm::quat &orientation, const glm::vec3 &position) :
    BulletPhysicsObject(shape, mass, orientation, position),
    poll_index_(0)
{
    hit_objects_.reserve(16);
}

BulletTriggerObject::BulletTriggerObject(const cl::BulletTriggerObject &obj) :
    BulletPhysicsObject(obj),
    hit_objects_(obj.hit_objects_)
{

}

BulletTriggerObject::~BulletTriggerObject()
{

}

BulletTriggerObject& BulletTriggerObject::operator=(const cl::BulletTriggerObject &obj)
{
    BulletPhysicsObject::operator=(obj);

    hit_objects_ = obj.hit_objects_;

    return *this;
}

bool BulletTriggerObject::intersected() const
{
    return !hit_objects_.empty();
}

bool BulletTriggerObject::pollHitObjects(TriggerCallbackData *output)
{
    if (poll_index_ >= hit_objects_.size())
        return false;

    *output = hit_objects_[poll_index_];
    poll_index_++;

    return true;
}

void BulletTriggerObject::clear()
{
    hit_objects_.clear();
    poll_index_ = 0;
}

std::vector<TriggerCallbackData>* BulletTriggerObject::getHitObjects()
{
    return &hit_objects_;
}

TriggerCallback::TriggerCallback(cl::BulletTriggerObject *trig) :
    btCollisionWorld::ContactResultCallback(),
    trigger(trig)
{
    
}

btScalar TriggerCallback::addSingleResult(btManifoldPoint &cp,
                                          const btCollisionObjectWrapper *colObj0, 
                                          int partId0, 
                                          int index0, 
                                          const btCollisionObjectWrapper *colObj1,
                                          int partId1,
                                          int index1)
{
    TriggerCallbackData data;

    if (colObj0->m_collisionObject == static_cast<btCollisionObject*>(trigger->getRigidBody()))
    {
        cl::PhysicAddresses *pa = static_cast<cl::PhysicAddresses*>(colObj1->m_collisionObject->getUserPointer());

        data.pointer = pa->trigger;
        data.index = colObj1->m_collisionObject->getUserIndex();

        trigger->getHitObjects()->push_back(data); 
    }
    else
    {
        cl::PhysicAddresses *pa = static_cast<cl::PhysicAddresses*>(colObj0->m_collisionObject->getUserPointer());

        data.pointer = pa->trigger;
        data.index = colObj0->m_collisionObject->getUserIndex();

        trigger->getHitObjects()->push_back(data); 
    }

    return 0;
}

}
