#include <Clone/Bullet/BulletCharacterObject.hpp>
#include <Clone/Bullet/BulletPhysicsWorld.hpp>

namespace cl
{

BulletCharacterObject::BulletCharacterObject(btCollisionShape *shape, float height, float mass, const glm::quat &orientation, const glm::vec3 &position) :
    BulletPhysicsObject(shape, mass, orientation, position),
    height_(height),
    step_size_(0.5f),
    jump_velocity_(0.0f, 20.0f, 0.0f),
    high_slope_(false),
    jumping_(false),
    on_ground_(false)
{
    getRigidBody()->setActivationState(DISABLE_DEACTIVATION);
    getRigidBody()->setAngularFactor(0.0f);
}

BulletCharacterObject::BulletCharacterObject(const BulletCharacterObject &obj) :
    BulletPhysicsObject(obj),
    height_(obj.height_),
    step_size_(obj.step_size_),
    jump_velocity_(obj.jump_velocity_),
    high_slope_(obj.high_slope_),
    jumping_(obj.jumping_),
    on_ground_(obj.on_ground_),
    surface_normal_(obj.surface_normal_)
{
    getRigidBody()->setActivationState(DISABLE_DEACTIVATION);
    getRigidBody()->setAngularFactor(0.0f);
}

BulletCharacterObject::~BulletCharacterObject()
{
    if (getParentWorld() != NULL)
        getParentWorld()->removeCharacter(this);
}

BulletCharacterObject& BulletCharacterObject::operator=(const BulletCharacterObject &obj)
{
    cl::BulletPhysicsObject::operator=(obj);

    height_ = obj.height_;
    step_size_ = obj.step_size_;
    jump_velocity_ = obj.jump_velocity_;
    high_slope_ = obj.high_slope_;
    jumping_ = obj.jumping_;
    on_ground_ = obj.on_ground_;
    surface_normal_ = obj.surface_normal_;

    getRigidBody()->setActivationState(DISABLE_DEACTIVATION);
    getRigidBody()->setAngularFactor(0.0f);

    return *this;
}

void BulletCharacterObject::preStep()
{
    if (high_slope_)
    {
        glm::vec3 u_axis = glm::normalize(glm::cross(surface_normal_, glm::vec3(0.0f, 1.0f, 0.0f)));
        glm::vec3 v_axis = glm::normalize(glm::cross(u_axis, surface_normal_));
        glm::vec3 vel_fix = 2.0f * v_axis / glm::dot(surface_normal_, glm::vec3(0.0f, 1.0f, 0.0f));
        setVelocity(getVelocity() - vel_fix);
    }

    setPosition(getPosition() + glm::vec3(0.0f, step_size_, 0.0f));
}

void BulletCharacterObject::postStep()
{
    glm::vec3 position = getPosition();
    btVector3 from(position.x, position.y, position.z);
    btVector3 to = from - btVector3(0.0f, height_ * 5.0f, 0.0f);

    // Give the object friction because it may have been removed while "in-air" / "jumping"
    getRigidBody()->setFriction(0.5f);

    cl::BulletRayCallback cb(from, to);
    getParentWorld()->getDynamicsWorld()->rayTest(from, to, cb);

    if (cb.hasHit())
    {
        glm::vec3 pos;
        pos.x = cb.m_hitPointWorld.getX();
        pos.y = cb.m_hitPointWorld.getY();
        pos.z = cb.m_hitPointWorld.getZ();

        glm::vec3 norm;
        norm.x = cb.m_hitNormalWorld.getX();
        norm.y = cb.m_hitNormalWorld.getY();
        norm.z = cb.m_hitNormalWorld.getZ();

        // Calculate how "upwards" the raycast surface is
        float dot = glm::dot(norm, glm::vec3(0.0f, 1.0f, 0.0f));

        // If the surface isn't perfectly flat, take a small factor of the dot and use it for offsetting the 
        // position. This will try to place the centre of the hitbox correctly on the ramp.
        float pos_factor = 1.0f - dot > 0.0f ? 0.5f * (1.0f - dot) : 0.0f;

        // A slightly higher amount than "factor". Meant to prevent a visible "snap" when falling on the ground.
        float snap_factor = 1.0f - dot > 0.0f ? 0.7f * (1.0f - dot) : 0.0f;

        bool going_upwards = jumping_ && getVelocity().y > 0.0f;

        // Because step_size_ has already been added due to the preStep(), we factor it into our calculations.
        // If the character is within his height multiplied by a dot product factor (for slopes), we "snap" him
        // to the intersection.
        //
        // As well, the 1.0f - dot < 0.4 tests if the surface is less than or equal to ~50 degrees, which 
        // is a suitable amount for games.
        if (!going_upwards && glm::length(pos - position) <= (height_ * (1.0f + snap_factor)) + step_size_ && 1.0f - dot < 0.4)
        {
            setPosition(pos + glm::vec3(0.0f, height_ + pos_factor, 0.0f));
            setVelocity(getVelocity() * glm::vec3(1.0f, 0.0f, 1.0f));

            high_slope_ = false;
            on_ground_ = true;
        }
        // The character isn't close enough to the surface to "snap", or tried to move above ~50 degree ramp
        else
        {
            // Tried to move above ~50 degree ramp
            if (1.0f - dot >= 0.4 && glm::length(pos - position) <= 1.2f*(height_ * (1.0f + snap_factor)) + step_size_)
            {
                /*
                high_slope_ = true;
                surface_normal_ = norm;
                on_ground_ = false;
                */
                on_ground_ = false;
            }
            else
            {
                high_slope_ = false;
                on_ground_ = false;

                // Character is in air, so we must remove friction so he doesn't stick to walls like spiderman
                getRigidBody()->setFriction(0.0f);
            }

            setPosition(position - glm::vec3(0.0f, step_size_, 0.0f));
        }
    }
    else
    {
        setPosition(position - glm::vec3(0.0f, step_size_, 0.0f));
        high_slope_ = false;
        on_ground_ = false;

        // Character is in air, so we must remove friction so he doesn't stick to walls like spiderman
        getRigidBody()->setFriction(0.0f);
    }

    if (jumping_ && on_ground_)
        jumping_ = false;
}

void BulletCharacterObject::jump()
{
    if (!jumping_ && !high_slope_)
    {
        setVelocity((getVelocity() * glm::vec3(1.0f, 0.0f, 1.0f)) + jump_velocity_);
        jumping_ = true;
    }
}

void BulletCharacterObject::setJumpVelocity(const glm::vec3 &jump_velocity)
{
    jump_velocity_ = jump_velocity;
}

glm::vec3 BulletCharacterObject::getJumpVelocity() const
{
    return jump_velocity_;
}

void BulletCharacterObject::setStepSize(float step_size)
{
    step_size_ = step_size;
}

float BulletCharacterObject::getStepSize() const
{
    return step_size_;
}

bool BulletCharacterObject::onGround() const
{
    return on_ground_;
}

void BulletCharacterObject::removeFromParentWorld()
{
    if (getParentWorld() != NULL)
        getParentWorld()->removeCharacter(this);
}

}
