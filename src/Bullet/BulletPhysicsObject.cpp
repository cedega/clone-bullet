#include <Clone/Bullet/BulletPhysicsObject.hpp>
#include <Clone/Bullet/BulletPhysicsWorld.hpp>

namespace cl
{

BulletPhysicsObject::BulletPhysicsObject(btCollisionShape *shape, float mass, const glm::quat &orientation, const glm::vec3 &position) :
    parent_world_(NULL),
    shape_(shape),
    mass_(mass)
{
    btVector3 local_inertia(0.0f, 0.0f, 0.0f);
    shape_->calculateLocalInertia(mass_, local_inertia);

    motion_state_.setWorldTransform(btTransform(btQuaternion(orientation.x, orientation.y, orientation.z, orientation.w), btVector3(position.x, position.y, position.z)));

    rigid_body_ = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(mass_, &motion_state_, shape_, local_inertia)); 

    addresses_.trigger = NULL;
    addresses_.physics = this;

    rigid_body_->setUserPointer(&addresses_);
}

BulletPhysicsObject::BulletPhysicsObject(const BulletPhysicsObject &obj) :
    parent_world_(obj.parent_world_),
    shape_(obj.shape_),
    motion_state_(obj.motion_state_),
    mass_(obj.mass_)
{
    btVector3 local_inertia(0.0f, 0.0f, 0.0f);
    shape_->calculateLocalInertia(mass_, local_inertia);

    rigid_body_ = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(mass_, &motion_state_, shape_, local_inertia)); 

    addresses_.trigger = obj.addresses_.trigger;
    addresses_.physics = this;

    rigid_body_->setUserPointer(&addresses_);
}

BulletPhysicsObject::~BulletPhysicsObject()
{
    deconstruct();
}

void BulletPhysicsObject::deconstruct()
{
    if (rigid_body_ != NULL)
    {
        removeFromParentWorld();

        delete rigid_body_;
        rigid_body_ = NULL;
    }
}

BulletPhysicsObject& BulletPhysicsObject::operator=(const BulletPhysicsObject &obj)
{
    parent_world_ = obj.parent_world_;
    shape_= obj.shape_;
    motion_state_ = obj.motion_state_;
    mass_ = obj.mass_;

    btVector3 local_inertia(0.0f, 0.0f, 0.0f);
    shape_->calculateLocalInertia(mass_, local_inertia);

    rigid_body_ = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(mass_, &motion_state_, shape_, local_inertia)); 

    addresses_.trigger = obj.addresses_.trigger;
    addresses_.physics = this;

    rigid_body_->setUserPointer(&addresses_);

    return *this;
}

void BulletPhysicsObject::setPosition(const glm::vec3 &position)
{
    btTransform transform = rigid_body_->getCenterOfMassTransform();
    transform.setOrigin(btVector3(position.x, position.y, position.z));
    rigid_body_->setCenterOfMassTransform(transform);
}

glm::vec3 BulletPhysicsObject::getPosition() const
{
    btVector3 vec = rigid_body_->getCenterOfMassTransform().getOrigin();
    glm::vec3 position;
    position.x = vec.getX();
    position.y = vec.getY();
    position.z = vec.getZ();

    return position;
}

void BulletPhysicsObject::setRotation(const glm::vec3 &rotation)
{
    glm::quat rot(rotation);
    btTransform transform = rigid_body_->getCenterOfMassTransform();
    transform.setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));
    rigid_body_->setCenterOfMassTransform(transform);
}

glm::vec3 BulletPhysicsObject::getRotation() const
{
    btQuaternion quat = rigid_body_->getOrientation();

    glm::quat rotation;
    rotation.x = quat.getX();
    rotation.y = quat.getY();
    rotation.z = quat.getZ();
    rotation.w = quat.getW();

    return glm::eulerAngles(rotation);
}

void BulletPhysicsObject::setVelocity(const glm::vec3 &velocity)
{
    rigid_body_->setLinearVelocity(btVector3(velocity.x, velocity.y, velocity.z));
}

glm::vec3 BulletPhysicsObject::getVelocity() const
{
    glm::vec3 velocity;
    velocity.x = rigid_body_->getLinearVelocity().getX();
    velocity.y = rigid_body_->getLinearVelocity().getY();
    velocity.z = rigid_body_->getLinearVelocity().getZ();
    return velocity;
}

void BulletPhysicsObject::setTransformMatrix(const glm::mat4 &matrix)
{
    btTransform trans;
    trans.setFromOpenGLMatrix(glm::value_ptr(matrix));
    rigid_body_->setCenterOfMassTransform(trans);
}

glm::mat4 BulletPhysicsObject::getTransformMatrix() const
{
    btTransform trans = rigid_body_->getCenterOfMassTransform();
    glm::mat4 m(1.0f);
    trans.getOpenGLMatrix(glm::value_ptr(m));

    return m;
}

void BulletPhysicsObject::setTriggerPointer(void *pointer)
{
    addresses_.trigger = pointer;
}

void* BulletPhysicsObject::getTriggerPointer()
{
    return addresses_.trigger;
}

void BulletPhysicsObject::setTriggerIndex(int index)
{
    rigid_body_->setUserIndex(index);
}

int BulletPhysicsObject::getTriggerIndex() const
{
    return rigid_body_->getUserIndex();
}

void BulletPhysicsObject::setGravity(const glm::vec3 &gravity)
{
    rigid_body_->setGravity(btVector3(gravity.x, gravity.y, gravity.z));
}

glm::vec3 BulletPhysicsObject::getGravity() const
{
    btVector3 g = rigid_body_->getGravity();
    return glm::vec3(g.getX(), g.getY(), g.getZ());
}

void BulletPhysicsObject::setLinearFactor(const glm::vec3 &linear_factor)
{
    rigid_body_->setLinearFactor(btVector3(linear_factor.x, linear_factor.y, linear_factor.z));
}

glm::vec3 BulletPhysicsObject::getLinearFactor() const
{
    btVector3 l = rigid_body_->getLinearFactor();
    return glm::vec3(l.getX(), l.getY(), l.getZ());
}

void BulletPhysicsObject::setAngularFactor(const glm::vec3 &angular_factor)
{
    rigid_body_->setAngularFactor(btVector3(angular_factor.x, angular_factor.y, angular_factor.z));
}

glm::vec3 BulletPhysicsObject::getAngularFactor() const
{
    btVector3 a = rigid_body_->getAngularFactor();
    return glm::vec3(a.getX(), a.getY(), a.getZ());
}

void BulletPhysicsObject::setDamping(float linear_damping, float angular_damping)
{
    rigid_body_->setDamping(linear_damping, angular_damping);
}

float BulletPhysicsObject::getLinearDamping() const
{
    return rigid_body_->getLinearDamping();
}

float BulletPhysicsObject::getAngularDamping() const
{
    return rigid_body_->getAngularDamping();
}

void BulletPhysicsObject::setMass(float mass)
{
    mass_ = mass;

    btVector3 local_inertia(0.0f, 0.0f, 0.0f);
    shape_->calculateLocalInertia(mass_, local_inertia);

    rigid_body_->setMassProps(mass_, local_inertia);
}

float BulletPhysicsObject::getMass() const
{
    return mass_;
}

void BulletPhysicsObject::setRestitution(float restitution)
{
    rigid_body_->setRestitution(restitution);
}

float BulletPhysicsObject::getRestitution() const
{
    return rigid_body_->getRestitution();
}

void BulletPhysicsObject::setFriction(float friction)
{
    rigid_body_->setFriction(friction);
}

float BulletPhysicsObject::getFriction() const
{
    return rigid_body_->getFriction();
}

void BulletPhysicsObject::setRollingFriction(float rolling_friction)
{
    rigid_body_->setRollingFriction(rolling_friction);
}

float BulletPhysicsObject::getRollingFriction() const
{
    return rigid_body_->getRollingFriction();
}

btRigidBody* BulletPhysicsObject::getRigidBody()
{
    return rigid_body_;
}

cl::BulletPhysicsWorld* BulletPhysicsObject::getParentWorld()
{
    return parent_world_;
}

void BulletPhysicsObject::removeFromParentWorld()
{
    if (parent_world_ != NULL)
    {
        parent_world_->removeObject(this);
        parent_world_ = NULL;
    }
}

}
