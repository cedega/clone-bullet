#include <Clone/Bullet/BulletDebugDraw.hpp>

namespace cl
{

BulletDebugDraw::BulletDebugDraw() :
    debug_mode_(0)
{

}

BulletDebugDraw::~BulletDebugDraw()
{

}

void BulletDebugDraw::initialize(cl::Shader *shader)
{
    shader_ = shader;
}

void BulletDebugDraw::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color)
{
    GLfloat buffer[6];
    buffer[0] = from.getX();
    buffer[1] = from.getY();
    buffer[2] = from.getZ();
    buffer[3] = to.getX();
    buffer[4] = to.getY();
    buffer[5] = to.getZ();
   
    vertex_object_.setPrimitiveType(GL_LINES);
    vertex_object_.upload(2, buffer, 6 * sizeof(GLfloat), 0, NULL, 0);
    vertex_object_.enable(0, GL_FLOAT, 3, 0, 0);
    vertex_object_.draw();
    vertex_object_.clearVertexPointers();
}

void BulletDebugDraw::drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int life_time, const btVector3 &color)
{
    // Unimplemented
}

void BulletDebugDraw::reportErrorWarning(const char *warning_string)
{
    SDL_Log("[Clone] (BulletDebugDraw) Error: %s", warning_string);
}

void BulletDebugDraw::draw3dText(const btVector3 &location, const char *text_string)
{
    // Unimplemented
}

void BulletDebugDraw::setDebugMode(int debug_mode)
{
    debug_mode_ = debug_mode;
}

int BulletDebugDraw::getDebugMode() const
{
    return debug_mode_;
}

}
